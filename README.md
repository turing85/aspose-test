# Build
```
./mvn clean package
```

# Execute
- Change to the `target` folder:

    `cd target`

- Execute
  
    `java -jar aspose-demo-1.0-SNAPSHOT-jar-with-dependencies.jar <path-to-input-file> <path-to-output-file>`
  
  e.g.:
  
    `java -jar aspose-demo-1.0-SNAPSHOT-jar-with-dependencies.jar ../src/main/resources/test.ifc ../src/main/resources/testx-90.svg`
  
This will generate the specified `.svg`-file

# Expected output
We need a drawing from the given perspective, but only objects visible from the given point of view 
should be shown. Something similar to:

![expected.png](src/main/resources/expected.png)

but as line drawing. Windows do not need to be transparent.

# Actual output
Was is generated is a wireframe of the whole object from the given perspective, including elements
that should not be visible:

![testx-90.svg](src/main/resources/testx-90.svg)

For example, on the left side of the building, the interior staircase is rendered. Likewise, 
interior walls and doors, as well as the exterior wall on the opposite site of the building ar 
rendered.

# Use case
We want to mark certain elements on the outside of the building, e.g. an exterior wall. The drawing
should assist site managers to locate the marked element.