package de.turing;

import com.aspose.cad.Image;
import com.aspose.cad.fileformats.ObserverPoint;
import com.aspose.cad.fileformats.ifc.IfcImage;
import com.aspose.cad.imageoptions.CadRasterizationOptions;
import com.aspose.cad.imageoptions.SvgOptions;

public class AsposeTest {

  public static void main(final String... args) {
    final String pathToIfc =  args[0];
    final IfcImage image = (IfcImage) Image.load(pathToIfc);

    final CadRasterizationOptions vectorOptions = new CadRasterizationOptions();
    vectorOptions.setPageWidth(1500);
    vectorOptions.setPageHeight(1500);
    vectorOptions.setObserverPoint(new ObserverPoint(-90, 0, 0));

    final SvgOptions svgOptions = new SvgOptions();
    svgOptions.setVectorRasterizationOptions(vectorOptions);
    image.save(args[1], svgOptions);
  }
}
